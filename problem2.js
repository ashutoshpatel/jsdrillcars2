function findLastCar(inventory) {

    // if the inventory array is empty then return null
    if(inventory.length === 0){
        return null;
    }

    // call filter method and get the data of last id
    const result = inventory.filter((obj) => {
        return obj.id == inventory.length;
    });

    return result[0];
    
    // using reduce method
    /*
    let result = inventory.reduce( function (acc, value) {
        acc = value;
        return acc;
    }, {});

    return result;
    */
}

// export the function
module.exports = findLastCar;