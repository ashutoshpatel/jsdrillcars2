function recallInformation(inventory, id) {

    // if the inventory array is empty then return null
    if(inventory.length === 0){
        return null;
    }

    // using reduce method
    const answer = inventory.reduce(function (acc, curr) {
        // if the current object id is 33 the we got the details
        if (curr.id == id) {
            // store the object in acc
            acc = curr;
        }
        // return the object
        return acc;
    }, {});

    return answer;       
}

// export the function
module.exports = recallInformation;