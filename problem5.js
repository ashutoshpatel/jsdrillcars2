function findCarOlderThan2000(inventory){

    // if the inventory array is empty then return null
    if(inventory.length === 0){
        return null;
    }

    // use filter method
    const result = inventory.filter( (data) => {
        // check if the current car year is older then 2000
        if(data.car_year < 2000){
            // the return it
            return data;
        }
    });

    return result;
}

// export the function
module.exports = findCarOlderThan2000;