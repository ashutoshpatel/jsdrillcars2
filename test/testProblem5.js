// importing
const findCarOlderThan2000 = require('../problem5.js');
const inventory = require('../carData.js');

// call the function and store the return output in result
const result = findCarOlderThan2000(inventory);

// if the result is null
if (result === null) {
    console.log("data not found");
}
else {
    // printing the output directly using console.log
    console.log(result);
    console.log(" Number of older cars: " + result.length);

    // print using loop
    // for (let index = 0; index < result.length; index++) {
    //     // get the current car
    //     const car = result[index];
    //     // print the output
    //     console.log(car.id + " " + car.car_make + " " + car.car_model + " " + car.car_year);
    // }
}