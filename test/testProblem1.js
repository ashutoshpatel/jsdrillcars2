// importing
const recallInformation = require('../problem1.js');
const inventory = require('../carData.js');

// call the function and store the return output in result
const result = recallInformation(inventory, 33);

// if the result is null
if(result === null){
    console.log("data not found");
}
else{
    // print the output
    console.log(`Car 33 is a ${result.car_year} ${result.car_make} ${result.car_model}`);
}