// importing
const findLastCar = require('../problem2.js');
const inventory = require('../carData.js');

// call the function and store the return output in result
const result = findLastCar(inventory);

// if the result is null
if(result === null){
    console.log("data not found");
}
else{
    // print the output
    console.log(`Last car is a ${result.car_make} ${result.car_model}`);
}