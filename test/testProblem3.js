// importing
const carModelListAlphabetically = require('../problem3.js');
const inventory = require('../carData.js');

// call the function and store the return output in result
const result = carModelListAlphabetically(inventory);

// if the result is null
if(result === null){
    console.log("data not found");
}
else{
    // printing the output
    console.log(result);
}