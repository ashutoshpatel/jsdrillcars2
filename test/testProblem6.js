// importing
const onlyBMWAndAudiCars = require('../problem6.js');
const inventory = require('../carData.js');

// call the function and store the return output in result
const result = onlyBMWAndAudiCars(inventory);

// if the result is null
if (result === null) {
    console.log("data not found");
}
else {
    // printing the output directly using console.log
    console.log(result);

    // print using loop
    // for (let index = 0; index < result.length; index++) {
    //     // get the current car
    //     const car = result[index];
    //     // print the output
    //     console.log(`id: ${car.id}, car_make: ${car.car_make}, car_model: ${car.car_model}, car_year: ${car.car_year}`);
    // }
}