// importing
const getAllTheYears = require('../problem4.js');
const inventory = require('../carData.js');

// call the function and store the return output in result
const result = getAllTheYears(inventory);

// if the result is null
if(result === null){
    console.log("data not found");
}
else{
    // printing the output
    console.log(result);
}