function carModelListAlphabetically(inventory){

    // if the inventory array is empty then return null
    if(inventory.length === 0){
        return null;
    }

    // use the map method and map only car_model
    const result = inventory.map( (value) => {
        return value.car_model;
    });
    
    // sort the data using in-built sorting method
    result.sort();

    return result;
}

// export the function
module.exports = carModelListAlphabetically;