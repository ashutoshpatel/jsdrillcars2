function onlyBMWAndAudiCars(inventory){

    // if the inventory array is empty then return null
    if(inventory.length === 0){
        return null;
    }

    // use filter method 
    const result = inventory.filter( (value) => {
        // check the current car_make is BMW or Audi
        if(value.car_make === 'BMW' || value.car_make === 'Audi'){
            // if yes then return the current car
            return value;
        }
    });

    return result;
}

// export the function
module.exports = onlyBMWAndAudiCars;