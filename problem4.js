function getAllTheYears(inventory) {

    // if the inventory array is empty then return null
    if(inventory.length === 0){
        return null;
    }

    // using the map method
    // call map method and return only car_year
    const answer = inventory.map((value) => {
        return value.car_year;
    });

    // using reduce method
    /*
    const answer = inventory.reduce(function (arr, current) {
        // push the car_year in array
        arr.push(current.car_year);
        return arr;
    }, []);
    */

    return answer;
}

// export the function
module.exports = getAllTheYears;